const BusModel = require("../models/busModel");
const BusTrip = require("../models/busTripModel");
const {
  addBus,
  isValidBusId,
  findBusByOperatorId,
  updateBusdb,
} = require("../Services/busDb");

exports.insert = async (req, res) => {
  // #swagger.tags = ['Bus']
  // #swagger.summary = 'returns message and bus id  '
  /* #swagger.requestBody = {
              required: true,
              content: {
                  "application/json": {
                      schema: { $ref: "#/definitions/createBus" }
                  }
              }
          }
    */
  try {
    console.log("Inside Insert Function");

    const bus = await addBus(req.body);
    console.log("insert bus " + bus);
    if (bus) {
      // #swagger.responses[200] = { description: 'Bus creation successfull', schema: { $ref: "#/definitions/BusCreateResponse" }}
      return res.status(200).json({
        message: "Bus creation successful",
        id: bus.id,
      });
    } else {
      // #swagger.responses[400] = { description: 'Bus creation  not successfull' }

      return res.status(401).json({
        message: "Bus creation not successful",
        error: "Error Ocurred" + `${err}`,
      });
    }
  } catch (error) {
    // #swagger.responses[406] = { description: 'Error in Bus creation' }
    return res.status(406).json({
      message: "Bus creation not successful",
      error: "Error Ocurred" + `${err}`,
    });
  }
};

exports.updateBus = async (req, res) => {
  // #swagger.tags = ['Bus']
  // #swagger.summary = 'returns message on sucessfull updation '
  /* #swagger.requestBody = {
              required: true,
              content: {
                  "application/json": {
                      schema: { $ref: "#/definitions/updateBus" }
                  }
              }
          }
    */
  try {
    const bus = await updateBusdb(req.params.id, req.body);

    // const bus =  await BusModel.findByIdAndUpdate()

    console.log("Bus details updated..." + "\n" + bus);

    return res.status(200).json({
      message: "Successfully Updated!!!",
    });
  } catch (err) {
    console.log("error : ", err);
    return res.status(400).json({
      err: "error in updating bus details",
    });
  }
};

exports.deleteBus = async (req, res) => {
  try {
    const bus = await BusModel.deleteOne({ _id: req.params.id });
    return res.status(200).json({
      message: "deleted the bus ",
    });
  } catch (err) {
    return res.status(400).json({
      err: "error in updating bus details",
    });
  }
};

exports.getAllBus = async (req, res) => {
  try {
    console.log(req.body.busOperatorId);

    const bus = await findBusByOperatorId(req.body.busOperatorId);

    if (bus == "") {
      return res.status(400).json({
        error: "You have not added bus",
      });
    }
    return res.status(200).json({ bus });
    console.log(bus);
  } catch (err) {
    console.log("error occuured in get all bus" + err);
  }
};

exports.getbus = async (req, res) => {
  // #swagger.tags = ['Bus']
  // #swagger.summary = 'returns message and bus id  '
  /* #swagger.requestBody = {
              required: true,
              content: {
                  "application/json": {
                      schema: { $ref: "#/definitions/createBus" }
                  }
              }
          }
    */
  try {
    // const bus = await BusModel.findOne({ _id: `${req.params.id}`})
    const bus = await isValidBusId(req.params.id);
    if (bus == "") {
      return res.status(400).json({
        error: "You have not added bus",
      });
    }
    return res.status(200).json({ bus });
    console.log(bus);
  } catch (err) {
    console.log("error occuured in get all bus" + err);
  }
};
