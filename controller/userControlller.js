const User = require("../models/user");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const { checkDetails, registerUser } = require("../Services/userDb");

//Register
exports.register = async (req, res) => {
  // #swagger.tags = ['User']
  // #swagger.summary = 'Returns message registration successful'
  /* #swagger.requestBody = {
              required: true,
              content: {
                  "application/json": {
                      schema: { $ref: "#/definitions/Register" }
                  }
              }
          }
    */

  try {
    const { name, mail, password, role } = req.body;
    const user = await registerUser({
      name,
      mail,
      password,
      role,
    });

    if (user) {
      // #swagger.responses[200] = { description: 'Registration successfull' , schema: { $ref: "#/definitions/RegistrationResponse" } }
      res.status(200).json({
        message: "User registration successfull",
      });
    }
  } catch (err) {
    return res.status(400).json({
      err: "error in registering",
      err: err.message,
    });
  }
};

//Login
exports.login = async (req, res) => {
  // #swagger.tags = ['User']
  // #swagger.summary = 'Returns message and token on login'
  /* #swagger.requestBody = {
              required: true,
              content: {
                  "application/json": {
                      schema: { $ref: "#/definitions/Login" }
                  }
              }
          }
    */
  try {
    const user = await checkDetails(req.body.mail);
    console.log(user);

    if (user && (await bcrypt.compare(req.body.password, user.password))) {
      const token = jwt.sign({ user }, process.env.SECRET_KEY || 'thisiskey', {
        expiresIn: "10d",
      });
      console.log(token);
      // #swagger.responses[200] = { description: 'Login successfull' , schema: { $ref: "#/definitions/LoginResponse" } }
      res.status(200).json({
        message: "welcome!!",
        token: token,
      });
    } else if (user == null) {
      // #swagger.responses[400] = { description: 'UserID/password incorrect!!' }
      res.status(400).json({
        err: "error in login",
        message: "User with this email does not exist.Please Register!!",
      });
    } else {
      // #swagger.responses[400] = { description: 'UserID/password incorrect!!' }
      res.status(400).json({
        err: "error in login",
        message: "UserID/password incorrect!!",
      });
    }
  } catch (err) {
    return res.status(400).json({
      err: "error in login",
      err: err.message,
    });
  }
};
