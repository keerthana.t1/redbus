const busModel = require("../models/busModel");
const BusTrip = require("../models/busTripModel");
const mongoose = require("mongoose");
const seatSchema = require("../models/seatModel");
const Seat = mongoose.model("seat", seatSchema);

const {createNewTrip,findTrip,findTripByOperator,deleteTrip,updateTripDb} = require("../Services/tripDb")
const {isValidBusId} = require('../Services/busDb')



const generateSeats = (totalSeats) =>{
    try {
        console.log("genertsing seats")
        let seats = [];
        for (let i = 1; i <= totalSeats; i++) {
            let seatNo = i;
            
            
            const seat = new Seat({ seatNo });
            seats.push(seat);
        }
        return seats;
    }
    catch(err){
        res.send(err)
    }
  

}

exports.addTrip= async (req,res) =>{
     // #swagger.tags = ['Trip']
  // #swagger.summary = 'Returns message and tripid on successful trip creation'
  // #swagger.description = 'Creates a new Trip'
  /* #swagger.parameters['Token'] = {
        in: 'header',
        description: 'Token of the user',
        required: 'true',
  } */
  /* #swagger.requestBody = {
              required: true,
              content: {
                  "application/json": {
                      schema: { $ref: "#/definitions/addTrip" }
                  }
              }
          }
    */

    try{
    console.log("Inside Bus AddTrip Function")

    const seats = generateSeats(req.body.totalSeats);


  
    
  
    const bus = new BusTrip({
        name:req.body.name,
        source:req.body.source,
        destination:req.body.destination,
        amount:req.body.amount,
        totalSeats:req.body.totalSeats,
        bookedSeats:req.body.bookedSeats,
        type:req.body.type,
        isAC:req.body.isAC,
        busOperatorId : req.body.busOperatorId,
        startDate : req.body.startDate,
        endDate : req.body.endDate,
        busId : req.params.id
        
    })


    bus.seat = seats;
    const trip = await createNewTrip(bus)
    
    
    
        if(trip) {
            // #swagger.responses[200] = { description: 'Trip creation successful' , schema: { $ref: "#/definitions/TripCreateResponse" }  }
            return res.status(200).json({
                message: "Trip creation successful",
                id: trip.id
              })
        }
        
        return res.status(401).json({
            // #swagger.responses[401] = { description: 'Trip creation not successful' }
            message: "Trip creation not successful",
            error: "Error Ocurred",
          })
    }
    catch(err){
        res.send(err)
    }

}


exports.getAllTrip = async(req,res) => {

    // #swagger.tags = ['Trip']
  // #swagger.summary = 'Returns a  list of  Trips  of given busid'
  // #swagger.description = 'Returns a  list of  Trips  of given busid if Valid id is given'
  /* #swagger.parameters['Token'] = {
        in: 'header',
        description: 'Token of User ',
        required: 'true',
  } */
   
    
    try {
    const busId = req.params.id
    const bus = await isValidBusId(busId)
    
    
    if(String(bus.busOperatorId) === String(req.auth.user._id)){
        const busTrip = await findTrip(busId) 
          // #swagger.responses[200] = { description: 'Returns Trips' , schema: { $ref: "#/definitions/GetBusTripsResponse" }   }
        return res.status(200).json({ busTrip});
    }
    else{
        // #swagger.responses[400] = { description: 'Invalid' }
        
        return res.status(400).json({
            message: "Your are not the operator"
           
          })
    }

    }
    catch(err){
        // #swagger.responses[401] = { description: 'Bad request' }
        return res.status(401).json({
            message: "Cannot find Trip",
            error: err.message,
          })

    }
    



}

exports.deleteTrip = async(req,res) =>{
      // #swagger.tags = ['Trip']
  // #swagger.summary = 'To delete the trip'
  // #swagger.description = 'Returns a  message on successful deletion of trip'
  /* #swagger.parameters['Token'] = {
        in: 'header',
        description: 'Token of User ',
        required: 'true',
  } */

    try{
        const bus = await deleteTrip({ _id: req.trip.id })
        if(bus){
             // #swagger.responses[200] = { description: 'success' , schema: { $ref: "#/definitions/BusDeleteResponse" } }
            return res.status(200).json({
                "message" :"deleted the trip "
            })
        }
    }
    catch(err){
        return res.status(400).json({
            // #swagger.responses[400] = { description: 'Error occured' }
            'err':"error in deleting bus details"
        })

    }
    


}

exports.getAllTripOperator = async(req,res) =>{
         // #swagger.tags = ['Trip']
  // #swagger.summary = 'Returns a  list of  Trips '
  // #swagger.description = 'Returns a  list of  Trips created by operator'
  /* #swagger.parameters['Token'] = {
        in: 'header',
        description: 'Token issued to the operator',
        required: 'true',
  } */

    try{
        console.log("printingggg")
        const operator = req.auth.user._id
        console.log(operator)
        
        const bus = await findTripByOperator(operator)
        
        if(bus==""){
            // #swagger.responses[400] = { description: 'Error occured' }
            return res.status(400).json({
                'message':"no trips added by you"
            })
        }
        else{
            // #swagger.responses[200] = { description: 'Sucess' , schema: { $ref: "#/definitions/GetBusTripsResponse" } }
            return res.status(200).json({bus});
        }






    }catch(err){
        res.send(err)
    }
}

exports.updateTrip = async(req,res) => {
     // #swagger.tags = ['Trip']
  // #swagger.summary = 'Returns message and tripid on successful trip creation'
  // #swagger.description = 'Creates a new Trip'
  /* #swagger.parameters['Token'] = {
        in: 'header',
        description: 'Token of the user',
        required: 'true',
  } */
  /* #swagger.requestBody = {
              required: true,
              content: {
                  "application/json": {
                      schema: { $ref: "#/definitions/updateTrip" }
                  }
              }
          }
    */
        
    
    try{
        const bus =  await updateTripDb(req.params.id,req.body)
        console.log("Bus trip updating..."+"\n")

        // #swagger.responses[200] = { description: 'sucess' , schema: { $ref: "#/definitions/TripUpdateResponse" }}
        return res.status(200).json({
            "message":"Successfully Updated!!!"
        })


    } catch(err){
        console.log('error : '+err);
        // #swagger.responses[400] = { description: 'Error occured' }
        return res.status(400).json({
            'err':"error in updating bus details"
        })
    }
}