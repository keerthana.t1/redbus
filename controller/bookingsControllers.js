let BookedPassenger = require("../models/bookedPassenger");
let BusTrip = require("../models/busTripModel");

const { findTripByQuery, findTripById, updateTripDb } = require("../Services/tripDb");
const { updateBusdb } = require("../Services/busDb");

const {
  createNewBookings,
  findByBookingId,
  deleteBooking,
  findBookingByUser,
} = require("../Services/bookingsDb");

const updateSeats = (seats, seatNo) => {
  for (let seat of seats) {
    for (let no of seatNo) {
      if (seat.seatNo === no) {
        seat.isBooked = !seat.isBooked;
      }
    }
  }

  return seats;
};

exports.bookBus = async (req, res) => {
  try {
    console.log("bookbus");
    console.log(req.bus);
    const trip_id = req.bus._id;
    const user_id = req.auth.user._id;
    const noOfSeats = req.query.noOfSeats;
    const fare = req.bus.amount;
    const totalAmount = noOfSeats * fare;
    const source = req.bus.source;
    const destination = req.bus.destination;
    const bus_name = req.bus.name;
    const user_name = req.auth.user.name;
    const bus_id = req.bus.busId;
    const startDate = req.bus.startDate;
    const endDate = req.bus.endDate;

    console.log(req.bus);

    const newTrip = await findTripById(trip_id);
    let seats = newTrip.bookedSeats + parseInt(noOfSeats);
    newTrip.bookedSeats = seats;
    newTrip.seat = req.seat;

    const updatedBus = await updateTripDb(trip_id, newTrip);
    const passenger = new BookedPassenger({
      user_name: user_name,
      user_id: user_id,
      bus_id: bus_id,
      source: source,
      destination: destination,
      totalAmount: totalAmount,
      noOfSeats: noOfSeats,
      trip_id: trip_id,
      startDate: startDate,
      endDate: endDate,
      bus_name: bus_name,
      seatNo: req.freeSeatNo,
    });

    const bookings = await createNewBookings(passenger);
    console.log(bookings);

    console.log("updta" + updatedBus);

    if (!updatedBus) {
      return res.send("error while updating the bus deatils");
    }
    res.status(200).json({
      message: "booking successfull ",
      id: bookings.id,
    });
  } catch (error) {
    res.status(400).json({
      message: "An error occurred",
      error: error.message,
    });
  }
};
//End of Book Bus

exports.cancelBus = async (req, res) => {
  try {
    console.log("cancellinggg...");
    const user_id = req.auth.user._id;
    const booking_id = req.params.id;
    console.log(booking_id);

    const obj = await findByBookingId(booking_id);
    console.log("obj : " + obj);
    console.log("allocated seats" + obj.seatNo);
    console.log(obj.user_id);
    console.log(user_id);

    if (String(obj.user_id) === String(user_id)) {
      console.log("passengre can cancel");

      const updateTrip = await findTripById(obj.trip_id);
      console.log(obj.trip_id);
      var seats = updateTrip.bookedSeats - parseInt(obj.noOfSeats);
      updateTrip.bookedSeats = seats;
      updateTrip.seat = updateSeats(updateTrip.seat, obj.seatNo);

      const updatedBus = await updateTripDb(obj.trip_id, updateTrip);

      await deleteBooking(booking_id);
      res.status(200).json({
        message: "Cancelled you booking",
      });
    } else {
      return res.status(400).json({
        message: "cannot Cancelled invalid user",
      });
    }
  } catch (err) {
    res.status(400).json({
      err: err.message,
      message: "An error occurred",
    });
  }
};
//End of Cancel Bus

exports.getAllBookings = async (req, res) => {
  // #swagger.tags = ['Booking']
  // #swagger.summary = 'to get all bookings of users '
  // #swagger.description = 'Returns list of bookings for user'
  /* #swagger.parameters['Token'] = {
        in: 'header',
        description: 'Token of user',
        required: 'true',
  } */

  try {
    const user_id = req.auth.user._id;
    const obj = await findBookingByUser(user_id);
    console.log(obj);
    if (obj.length == 0) {
      // #swagger.responses[400] = { description: "NO data" }
      res.status(400).json({
        message: "No Bookings",
      });
    } else {
      // #swagger.responses[200] = { description: "success" , { $ref: "#/definitions/BookingsResponse" } }
      res.status(200).json({
        obj,
      });
    }
  } catch (err) {
    console.log(err);
  }
};
// End of getAllBooking

exports.searchByPlace = async (req, res) => {
  // #swagger.tags = ['Booking']
  // #swagger.summary = 'to search trips '
  // #swagger.description = 'Returns list of trips available for the given details'
  /* #swagger.parameters['source'] = {
        in: 'query',
        description: 'Source',
        required: 'true',
  } */
  /* #swagger.parameters['destination'] = {
        in: 'query',
        description: 'Destination',
        required: 'true',
  } */
  /* #swagger.parameters['Token'] = {
        in: 'header',
        description: 'Token of user',
        required: 'true',
  } */
  /* #swagger.parameters['type'] = {
        
        description: 'type of bus',
       
  } */
  /* #swagger.parameters['isAC'] = {
    
        in : boolean,
        description: 'AC/NON-AC'
        
  } */

  try {
    let query = req.value;
    const bus = await findTripByQuery(query);
    console.log("bus querying " + bus);
    if (bus == "") {
      // #swagger.responses[400] = { description: 'No data'  }
      return res.status(400).json({
        message: "No Buses Available !!!",
      });
    }
    console.log("Query Result: " + bus);
    // #swagger.responses[200] = { description: 'success' }
    return res.status(200).json({ bus });
  } catch (err) {
    res.send("ERROR" + err);
  }
};
