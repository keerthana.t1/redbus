const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;
const Seat = require("./seatModel");

const busTrip = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  source: {
    type: String,
    required: true,
  },
  destination: {
    type: String,
    required: true,
  },
  amount: {
    type: Number,
    required: true,
  },
  totalSeats: {
    type: Number,
    reuired: true,
  },
  bookedSeats: {
    type: Number,
  },
  type: {
    type: String,
    enum: ["NORMAL", "SLEEPER", "SEMI-SLEEPER"],
    required: true,
  },
  isAC: {
    type: Boolean,
    required: true,
  },
  busOperatorId: {
    type: ObjectId,
    ref: "User",
  },
  startDate: {
    type: Date,
  },
  endDate: {
    type: Date,
  },
  busId: {
    type: ObjectId,
    ref: "Bus",
    required: true,
  },
  seat: [
    {
      type: Seat,
    },
  ],
});

module.exports = mongoose.model("BusTrip", busTrip);
