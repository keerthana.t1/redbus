const mongoose = require("mongoose");

const seatSchema = new mongoose.Schema({
  seatNo: {
    type: Number,
    required: true,
  },

  isBooked: {
    type: Boolean,
    default: false,
  },
});

module.exports = seatSchema;
