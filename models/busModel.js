const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;

const busSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  source: {
    type: String,
    required: true,
  },
  destination: {
    type: String,
    required: true,
  },
  amount: {
    type: Number,
    required: true,
  },

  type: {
    type: String,
    enum: ["NORMAL", "SLEEPER", "SEMI-SLEEPER"],
    required: true,
  },
  isAC: {
    type: Boolean,
    required: true,
  },
  busOperatorId: {
    type: ObjectId,
    ref: "User",
  },
  registrationNo: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("Bus", busSchema);
