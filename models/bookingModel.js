const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;

const bookingSchema = new mongoose.Schema({
  user_id: {
    type: ObjectId,
    ref: "User",
  },
  bus_id: {
    type: ObjectId,
    ref: "Bus",
  },

  totalAmount: {
    type: Number,
    required: true,
  },

  noOfSeats: {
    type: Number,
    required: true,
  },
});

module.exports = mongoose.model("BookingModel", bookingSchema);
