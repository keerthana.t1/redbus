const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;

const passengerSchema = new mongoose.Schema({
  user_name: {
    type: String,
  },
  user_id: {
    type: ObjectId,
    ref: "User",
  },
  bus_id: {
    type: ObjectId,
    ref: "Bus",
  },
  source: {
    type: String,
    required: true,
  },
  destination: {
    type: String,
    required: true,
  },

  totalAmount: {
    type: Number,
    required: true,
  },

  noOfSeats: {
    type: Number,
    required: true,
  },
  trip_id: {
    type: ObjectId,
    ref: "BusTrip",
  },
  startDate: {
    type: Date,
  },

  endDate: {
    type: Date,
  },
  bus_name: {
    type: String,
  },
  seatNo: {
    type: Array,
    required: true,
  },
});

module.exports = mongoose.model("passenger", passengerSchema);
