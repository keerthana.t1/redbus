const mongoose = require('mongoose')
const bcrypt = require('bcrypt')


const userSchema = new mongoose.Schema({

    name : {
        type : String,
        required : true
    },
    mail :{
        type : String,
        required: true
    },
    password :{
        type : String,
        required: true,
        
    },
    role:{
        type: String,
        enum : ['customer','busOperator'],
        required: true
    }
})


userSchema.pre('save', async function(next) {
    try{
        
        const hashedPassword = await bcrypt.hash(this.password,10)
        this.password = hashedPassword

    }
    catch(err){
        console.log("error in hshing"+err)
    }
})

module.exports = mongoose.model('User',userSchema) 
