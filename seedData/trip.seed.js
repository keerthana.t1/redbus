
exports.busTrip = {
     name : "AHI travels",
     source : "coimbatore",
     destination : "chennai",
     amount : 400,
     type : "SEMI-SLEEPER",
     isAC : true,
     totalSeats : 32,
     bookedSeats :0,
     startDate : "2022-09-20T12:01:00.000Z",
     endDate  : "2022-09-23T12:30:00.000Z"

}


exports.busTrip2 = {
     name : "AHI travels",
     source : "coimbatore",
     destination : "chennai",
     amount : 400,
     type : "SEMI-SLEEPER",
     isAC : true,
     totalSeats : 32,
     bookedSeats :0,
     startDate : "2022-09-23T12:01:00.000Z",
     endDate  : "2022-09-25T12:30:00.000Z"

}





exports.busTrip1 = {
     name : "AHI travels",
     source : "coimbatore",
     destination : "chennai",
     amount : 400,
     type : "SEMI-SLEEPER",
     isAC : true,
     totalSeats : 32,
     bookedSeats :0,
     startDate : "2022-11-22T12:01:00.000Z",
     endDate  : "2022-11-23T12:30:00.000Z"

}


exports.busTripOverLap = {
     name : "AHI travels",
     source : "coimbatore",
     destination : "chennai",
     amount : 400,
     type : "SEMI-SLEEPER",
     isAC : true,
     totalSeats : 32,
     bookedSeats :0,
     startDate : "2022-09-21T12:01:00.000Z",
     endDate  : "2022-09-23T12:30:00.000Z"

}


exports.invalidBusTrip = {
     name : "AHI travels",
     source : "coimbatore",
     destination : "",
     amount : 400,
     type : "",
     isAC : true,
     totalSeats : 32,
     bookedSeats :0,
     startDate : "2022-09-21T12:01:00.000Z",
     endDate  : "2022-09-23T12:30:00.000Z"

}


exports.updateTrip = {

     name : "AHI travels",
     source : "coimbatore",
     destination : "chennai",
     amount : 500,
     type : "SEMI-SLEEPER",
     isAC : true,
     totalSeats : 32,
     bookedSeats :0,
     startDate : "2022-09-20T12:01:00.000Z",
     endDate  : "2022-09-21T12:30:00.000Z"



}


exports.seatsBookedTrip = {
     name : "AHI travels",
     source : "coimbatore",
     destination : "chennai",
     amount : 500,
     type : "SEMI-SLEEPER",
     isAC : true,
     totalSeats : 32,
     bookedSeats :15,
     startDate : "2022-10-20T12:01:00.000Z",
     endDate  : "2022-10-21T12:30:00.000Z"


}


exports.overlapUpdate = {
     name : "AHI travels",
     source : "coimbatore",
     destination : "chennai",
     amount : 400,
     type : "SEMI-SLEEPER",
     isAC : true,
     totalSeats : 32,
     bookedSeats :0,
     startDate : "2022-09-22T12:01:00.000Z",
     endDate  : "2022-09-24T11:30:00.000Z"

}
