const BusTrip = require("../models/busTripModel");

exports.checkTrip = async (busId, currStartDate, currEndDate) => {
  try {
    const trip = await BusTrip.findOne({
      busId: busId,
      $and: [
        {
          startDate: {
            $gte: currStartDate,
            $lte: currEndDate,
          },
        },
        {
          currentEndDate: {
            $gte: currStartDate,
            $lte: currEndDate,
          },
        },
      ],
    });
    console.log(trip);
    return trip;
  } catch (e) {
    return false;
  }
};

exports.createNewTrip = async (trip) => {
  try {
    const obj = await BusTrip.create(trip);
    return obj;
  } catch (error) {
    return false;
  }
};

exports.findTrip = async (busId) => {
  try {
    const busTrip = await BusTrip.find({ busId: `${busId}` });
    return busTrip;
  } catch (error) {
    return false;
  }
};

exports.findTripByOperator = async (operatorId) => {
  try {
    console.log("calling");
    const busTrip = await BusTrip.find({ busOperatorId: `${operatorId}` });

    return busTrip;
  } catch (error) {
    return false;
  }
};

exports.findTripById = async (id) => {
  try {
    console.log("calling");
    const trip = await BusTrip.findById(id);

    return trip;
  } catch (error) {
    return false;
  }
};

exports.deleteTrip = async (id) => {
  try {
    console.log("calling");
    const trip = await BusTrip.deleteOne({ _id: id });

    return trip;
  } catch (error) {
    return false;
  }
};

exports.updateTripDb = async (id, trip) => {
  try {
    const updatedTrip = await BusTrip.findByIdAndUpdate(id, trip);
    let dummy = await BusTrip.findById(id);
    console.log("adsasdasdasd" + dummy);
    return updatedTrip;
  } catch (error) {
    return trip;
  }
};

exports.findTripByQuery = async (query) => {
  try {
    const trip = BusTrip.find(query).exec();

    return trip;
  } catch (error) {
    return trip;
  }
};
