const BusModel = require("../models/busModel");

exports.findBus = async (registrationNo) => {
  try {
    const bus = await BusModel.find({ registrationNo: `${registrationNo}` });
    return bus;
  } catch (error) {
    return false;
  }
};

exports.addBus = async (bus) => {
  try {
    const obj = await BusModel.create(bus);
    return obj;
  } catch (error) {
    return false;
  }
};

exports.isValidBusId = async (id) => {
  try {
    const bus = await BusModel.findById({ _id: `${id}` }).exec();
    console.log(bus);
    return bus;
  } catch (error) {
    return false;
  }
};

exports.findBusByOperatorId = async (id) => {
  try {
    const bus = await BusModel.find({ busOperatorId: `${id}` });
    return bus;
  } catch (error) {
    return false;
  }
};

exports.updateBusdb = async (id, obj) => {
  try {
    const bus = await BusModel.findByIdAndUpdate(id, obj);
    console.log("bus db " + bus);
    return bus;
  } catch (error) {
    return false;
  }
};

exports.deleteBusdb = async (id) => {
  try {
    const bus = await BusModel.deleteOne({ _id: id });
    return true;
  } catch (error) {
    return false;
  }
};
