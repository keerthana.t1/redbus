const User = require("../models/user");

exports.registerUser = async (user) => {
  try {
    const obj = await User.create(user);
    return obj;
  } catch (e) {
    return false;
  }
};

exports.checkDetails = async (mail) => {
  try {
    console.log(mail);
    const object = await User.findOne({ mail: `${mail}` });
    console.log(object);
    return object;
  } catch (error) {
    console.log(error)
    return false;
  }
};
