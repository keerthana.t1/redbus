const BookedPassenger = require("../models/bookedPassenger");

exports.createNewBookings = async (value) => {
  try {
    const obj = await BookedPassenger.create(value);
    return obj;
  } catch (error) {
    return false;
  }
};

exports.findByBookingId = async (id) => {
  try {
    const obj = await BookedPassenger.findById(id);
    console.log(obj);
    return obj;
  } catch (error) {
    console.log(error);
  }
};

exports.deleteBooking = async (id) => {
  try {
    const obj = await BookedPassenger.deleteOne({ _id: `${id}` });
    console.log(obj);
    return true;
  } catch (error) {
    console.log(error);
  }
};

exports.findBookingByUser = async (id) => {
  try {
    const obj = await BookedPassenger.find({ user_id: `${id}` });
    console.log(obj);
    return obj;
  } catch (error) {
    console.log(error);
  }
};
