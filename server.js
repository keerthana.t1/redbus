const app = require("./app")
const {connect} = require("./dbConnect")
const {PORT} = process.env;



app.listen(PORT, function(){
    console.log('server started...')
    connect()
})