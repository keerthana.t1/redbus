const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");

dotenv.config({ path: "./config.env" });

const app = express();
const url = process.env.MONGODB_URL;
const con = mongoose.connection;

// try{
// mongoose.connect(url , {useNewUrlParser:true})

// }
// catch(err){
//     console.log(console.error())
// }

//  con.on('open',function(){
//     console.log("connected...")
// })

app.use(express.json());

// app.listen(process.env.PORT, function(){
//     console.log('server started...')
// })

// app.get('/test', (req,res)=> {
//     res.send("testing")
//     res.status(200)

// })



const usersRouter = require("./routes/usersRoute");
app.use("/users", usersRouter);

const trip = require("./routes/tripRoute");
app.use("/trips", trip);

const bus = require("./routes/busRoute");
app.use("/bus", bus);

const booking = require("./routes/bookingRoute");
app.use("/booking", booking);

const swaggerUi = require("swagger-ui-express");
const swaggerFile = require("./swagger_output.json");
app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerFile));

app.use("/", (req,res)=> res.send(" Success !"))

module.exports = app;
