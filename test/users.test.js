const app = require("../app");
const request = require("supertest");
const mongoose = require("mongoose");
// require("../dbConnect").connect()

const { connectDB, disconnectDB } = require("../dbConnectTest");
const {
  operator,
  newValidUser,
  newInvalidUser,
  validLoginOperator,
  invalidLogin,
} = require("../seedData/user.seed");

describe("Testing Users", () => {
  beforeAll(async () => {
    await connectDB();
    await request(app).post("/users/register").send(operator);
    console.log(operator);
  });

  afterAll(async () => {
    await disconnectDB();
  });

  describe("Test the user resgisteration", () => {
    test("If user name, mail password and role are given and mailid is unique", async () => {
      const response = await request(app)
        .post("/users/register")
        .send(newValidUser);
      expect(response.status).toBe(200);
    });
    test("If user name, mail password and role are given and mailid is not unique", async () => {
      const response = await request(app)
        .post("/users/register")
        .send(operator);
      expect(response.status).toBe(400);
    });
    test("If any/all details not given ", async () => {
      const response = await request(app)
        .post("/users/register")
        .send(newInvalidUser);

      expect(response.status).toBe(406);
    });
  });

  describe("Test the user Login", () => {
    test("If user mail password are given and it is valid", async () => {
      const response = await request(app)
        .post("/users/login")
        .send(validLoginOperator);

      expect(response.status).toBe(200);
      expect(response.body.token).toBeDefined();
    });
    test("If user mail password are given and it is not valid", async () => {
      const response = await request(app)
        .post("/users/login")
        .send(invalidLogin);

      expect(response.status).toBe(400);
    });

    test("If invalid mail is given ", async () => {
      const response = await request(app)
        .post("/users/login")
        .send(invalidLogin);

      expect(response.status).toBe(400);
    });

    test("If no mail is given ", async () => {
      const response = await request(app)
        .post("/users/login")
        .send(invalidLogin);

      expect(response.status).toBe(400);
    });
  });
});
