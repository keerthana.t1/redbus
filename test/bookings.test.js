const app = require("../app");
const request = require("supertest");
const mongoose = require("mongoose");
const { connectDB, disconnectDB } = require("../dbConnectTest");

const {
  operator,
  validLoginOperator,
  customer,
  validLoginCustomer,
} = require("../seedData/user.seed");

const { busSeed } = require("../seedData/bus.seed");

const { busTrip } = require("../seedData/trip.seed");

describe("Testing for Bookings", () => {
  let operatorToken = null;
  let customerToken = null;
  
  let busID = null;
  let tripID = null;
 
  let bookingID = null;
  let seats = 2;
  let invalidSeats = 100;
  let invalidTripID = "6304b38edafedb48c80e0d48";

  beforeAll(async () => {
    await connectDB();
    await request(app).post("/users/register").send(operator);
    const loggedOpperator = await request(app)
      .post("/users/login")
      .send(validLoginOperator);
    operatorToken = loggedOpperator.body.token;

    await request(app).post("/users/register").send(customer);
    const loggedCustomer = await request(app)
      .post("/users/login")
      .send(validLoginCustomer);
    customerToken = loggedCustomer.body.token;

    const bus = await request(app)
      .post("/bus")
      .set("Token", operatorToken)
      .send(busSeed);
    busID = bus.body.id;

    const trip = await request(app)
      .post(`/trips/addTrip/${busID}`)
      .set("Token", operatorToken)
      .send(busTrip);
    tripID = trip.body.id;
    console.log(trip);

    const booking = await request(app)
      .post(`/booking/bookBus?tripId=${tripID}&noOfSeats=${seats}`)
      .set("Token", operatorToken);
    bookingID = booking.body.id;
    console.log("booking id " + bookingID);
    console.log("trip id " + tripID);
  });

  afterAll(async () => {
    await disconnectDB();
  });

  describe("Testing the searching buses ", () => {
    test("If all filters are given and buses are available ", async () => {
      const response = await request(app)
        .get(
          "/booking/?type=SEMI-SLEEPER&isAC=true&source=coimbatore&destination=chennai"
        )
        .set("Token", operatorToken);

      expect(response.status).toBe(200);
      expect(response.body.bus).toBeDefined();
    });

    test("If all filters are given and buses are not available ", async () => {
      const response = await request(app)
        .get(
          "/booking/?type=SLEEPER&isAC=true&source=coimbatore&destination=chennai"
        )
        .set("Token", operatorToken);

      expect(response.status).toBe(400);
      expect(response.body.message).toBeDefined();
    });
    test("If source and destination is not given", async () => {
      const response = await request(app)
        .get("/booking")
        .set("Token", operatorToken);

      console.log(response.body.bus);
      expect(response.status).toBe(400);
      expect(response.body.message).toBeDefined();
    });
  });

  describe("Testing for getting bookings", () => {
    test("If logged in and booked trips ", async () => {
      const response = await request(app)
        .get("/booking/getAllBookings")
        .set("Token", operatorToken);

      console.log(JSON.stringify(response.body.message));
      expect(response.status).toBe(200);
      expect(response.body.obj).toBeDefined();
    });

    test("If logged in and no booked trips ", async () => {
      const response = await request(app)
        .get("/booking/getAllBookings")
        .set("Token", customerToken);

      expect(response.status).toBe(400);
      expect(response.body.message).toBeDefined();
    });
    test("If not logged in ", async () => {
      const response = await request(app)
        .get("/booking/getAllBookings")
        .set("Token", "");

      expect(response.status).toBe(403);
      expect(response.body.error).toBeDefined();
    });
  });

  describe("Testing the booking buses ", () => {
    test("If logged in , valid tripid , seats are avalible  ", async () => {
      const response = await request(app)
        .post(`/booking/bookBus?tripId=${tripID}&noOfSeats=${seats}`)
        .set("Token", customerToken);

      expect(response.status).toBe(200);
      expect(response.body.message).toBeDefined();
      expect(response.body.id).toBeDefined();
    });

    test("If logged in , valid tripid , seats are not avalible  ", async () => {
      const response = await request(app)
        .post(`/booking/bookBus?tripId=${tripID}&noOfSeats=${invalidSeats}`)
        .set("Token", customerToken);

      expect(response.status).toBe(403);
      expect(response.body.message).toBeDefined();
    });

    test("If logged in , invalid tripid  ", async () => {
      const response = await request(app)
        .post(`/booking/bookBus?tripId=${invalidTripID}&noOfSeats=${seats}`)
        .set("Token", customerToken);

      expect(response.status).toBe(400);
      expect(response.body.message).toBeDefined();
    });

    test("If logged not logged in ", async () => {
      const response = await request(app)
        .post(`/booking/bookBus?tripId=${tripID}&noOfSeats=${seats}`)
        .set("Token", "");

      expect(response.status).toBe(403);
      expect(response.body.error).toBeDefined();
    });
  });

  describe("Testing the Cancelling booking ", () => {
    test("If logged in , valid bookingid , trip not ended , authorized to cancel   ", async () => {
      const response = await request(app)
        .delete(`/booking/cancelBus/${bookingID}`)
        .set("Token", operatorToken);

      expect(response.status).toBe(200);
      expect(response.body.message).toBeDefined();
    });

    test("If logged in , valid bookingid , trip not ended , not authorized to cancel   ", async () => {
      const response = await request(app)
        .delete(`/booking/cancelBus/${bookingID}`)
        .set("Token", customerToken);

      expect(response.status).toBe(400);
      expect(response.body.message).toBeDefined();
    });

    // test("If logged in , valid bookingid , trip ended ", async () => {

    //   const response = await request(app).delete(`/booking/cancelBus/${bookingID}`)
    //   .set("Token",customerToken)

    //   expect(response.status).toBe(200);
    //   expect(response.body.message).toBeDefined();

    // });

    test("If logged not logged in ", async () => {
      const response = await request(app)
        .delete(`/booking/cancelBus/${bookingID}`)
        .set("Token", "");

      expect(response.status).toBe(403);
      expect(response.body.error).toBeDefined();
    });
  });
});
