const app = require("../app");
const request = require("supertest");

const { connectDB, disconnectDB } = require("../dbConnectTest");

const {
  operator,
  validLoginOperator,
  customer,
  validLoginCustomer,
  operator1,
  validLoginOperator1,
} = require("../seedData/user.seed");

const {
  
  busSeed,
 
} = require("../seedData/bus.seed");

const {
  busTrip,
  busTrip1,
  busTripOverLap,
  invalidBusTrip,
  updateTrip,
  seatsBookedTrip,
  overlapUpdate,
  busTrip2,
} = require("../seedData/trip.seed");

describe("testing for trip ", () => {
  let operatorToken = null;
  let customerToken = null;
  let operator1Token = null;
  let busID = null;
  let tripID = null;
  let trip1ID = null;

  beforeAll(async () => {
    await connectDB();
    await request(app).post("/users/register").send(operator);
    const loggedOpperator = await request(app)
      .post("/users/login")
      .send(validLoginOperator);
    operatorToken = loggedOpperator.body.token;

    await request(app).post("/users/register").send(operator1);
    const loggedOperator1 = await request(app)
      .post("/users/login")
      .send(validLoginOperator1);
    operator1Token = loggedOperator1.body.token;

    await request(app).post("/users/register").send(customer);
    const loggedCustomer = await request(app)
      .post("/users/login")
      .send(validLoginCustomer);
    customerToken = loggedCustomer.body.token;
    console.log(customerToken);

    const bus = await request(app)
      .post("/bus")
      .set("Token", operatorToken)
      .send(busSeed);
    busID = bus.body.id;
    console.log(busID);

    const trip = await request(app)
      .post(`/trips/addTrip/${busID}`)
      .set("Token", operatorToken)
      .send(busTrip);
    tripID = trip.body.id;

    await request(app)
      .post(`/trips/addTrip/${busID}`)
      .set("Token", operatorToken)
      .send(busTrip2);

    const trip1 = await request(app)
      .post(`/trips/addTrip/${busID}`)
      .set("Token", operatorToken)
      .send(seatsBookedTrip);
    trip1ID = trip1.body.id;
  });

  afterAll(async () => {
    await disconnectDB();
  });

  describe("Testing get all trip by its bus id ", () => {
    test("User is logged in , role is operator and valid authurization for the given bus id", async () => {
      const response = await request(app)
        .get(`/trips/getAllTrip/${busID}`)
        .set("Token", operatorToken);

      expect(response.status).toBe(200);
      expect(response.body.busTrip).toBeDefined();
    });

    test("User is logged in , role is operator and invalid authurization for the given bus id", async () => {
      const response = await request(app)
        .get(`/trips/getAllTrip/${busID}`)
        .set("Token", operator1Token);

      expect(response.status).toBe(400);
      expect(response.body.message).toBeDefined();
    });

    test("User is logged in , role is customer", async () => {
      const response = await request(app)
        .get(`/trips/getAllTrip/${busID}`)
        .set("Token", customerToken);

      expect(response.status).toBe(403);
      expect(response.body.message).toBeDefined();
    });

    test("User is not logged in", async () => {
      const response = await request(app)
        .get(`/trips/getAllTrip/${busID}`)
        .set("Token", "");

      expect(response.status).toBe(403);
      expect(response.body.error).toBeDefined();
    });
  });

  describe("Testing for adding a trip", () => {
    test(" Authenticated to add bus ,Valid Details are given , Dates donot overlap", async () => {
      const response = await request(app)
        .post(`/trips/addTrip/${busID}`)
        .set("Token", operatorToken)
        .send(busTrip1);
      expect(response.status).toBe(200);
      expect(response.body).toBeDefined();
    });

    test(" Authenticated to add bus ,Valid Details are given , Dates overlap", async () => {
      const response = await request(app)
        .post(`/trips/addTrip/${busID}`)
        .set("Token", operatorToken)
        .send(busTripOverLap);
      expect(response.status).toBe(400);
      expect(response.body.message).toBeDefined();
    });

    test(" Authenticated to add bus ,Valid Details are not  given , ", async () => {
      const response = await request(app)
        .post(`/trips/addTrip/${busID}`)
        .set("Token", operatorToken)
        .send(invalidBusTrip);
      expect(response.status).toBe(400);
      expect(response.body.message).toBeDefined();
    });

    test(" Not Authenticated to add bus  ", async () => {
      const response = await request(app)
        .post(`/trips/addTrip/${busID}`)
        .set("Token", operator1Token)
        .send(busTrip1);
      expect(response.status).toBe(400);
      expect(response.body.error).toBeDefined();
    });
  });

  describe("Testing get all trip by its operator", () => {
    test("User is logged in , role is operator and added trips", async () => {
      const response = await request(app)
        .get("/trips/getAllTripOperator")
        .set("Token", operatorToken);

      expect(response.status).toBe(200);
      expect(response.body.bus).toBeDefined();
    });

    test("User is logged in , role is operator not added trips", async () => {
      const response = await request(app)
        .get("/trips/getAllTripOperator")
        .set("Token", operator1Token);

      expect(response.status).toBe(400);
      expect(response.body.message).toBeDefined();
    });

    test("User is  not logged in ", async () => {
      const response = await request(app)
        .get("/trips/getAllTripOperator")
        .set("Token", "");

      expect(response.status).toBe(403);
      expect(response.body.error).toBeDefined();
    });
  });

  describe("Testing for updating trip", () => {
    test("Authenticated to update bus ,Valid Details are given , Dates donot overlap , no tickets booked for trip ,", async () => {
      const response = await request(app)
        .put(`/trips/updateTrip/${tripID}`)
        .set("Token", operatorToken)
        .send(updateTrip);
      expect(response.status).toBe(200);
      expect(response.body.message).toBeDefined();
    });

    test(" Authenticated to update bus ,Valid Details are given , Dates donot overlap ,  tickets booked for trip ,", async () => {
      const response = await request(app)
        .put(`/trips/updateTrip/${trip1ID}`)

        .set("Token", operatorToken);

      expect(response.status).toBe(400);
      expect(response.body.message).toBeDefined();
    });
    test(" Authenticated to update bus ,Valid Details are not given", async () => {
      const response = await request(app)
        .put(`/trips/updateTrip/${tripID}`)
        .set("Token", operatorToken)
        .send(invalidBusTrip);
      expect(response.status).toBe(400);
      expect(response.body.message).toBeDefined();
    });

    test(" Authenticated to update bus ,Valid Details given , dates overlap with other trip of same bus", async () => {
      const response = await request(app)
        .put(`/trips/updateTrip/${tripID}`)
        .set("Token", operatorToken)
        .send(overlapUpdate);
      expect(response.status).toBe(400);
      expect(response.body.message).toBeDefined();
    });

    test("not  Authenticated to update bus ", async () => {
      const response = await request(app)
        .put(`/trips/updateTrip/${tripID}`)
        .set("Token", operator1Token)
        .send(busTrip1);
      expect(response.status).toBe(400);
      expect(response.body.message).toBeDefined();
    });
  });

  describe("Testing for deleting trip", () => {
    test("User is logged in , role is operator , Authenticated to deleted bus , no tickets booked for trip ,", async () => {
      const response = await request(app)
        .delete(`/trips/deleteTrip/${tripID}`)
        .set("Token", operatorToken);

      expect(response.status).toBe(200);
      expect(response.body.message).toBeDefined();
    });

    test("User is logged in , role is operator , Authenticated to deleted bus , tickets booked for trip ,", async () => {
      const response = await request(app)
        .delete(`/trips/deleteTrip/${trip1ID}`)
        .set("Token", operatorToken);

      expect(response.status).toBe(400);
      expect(response.body).toBeDefined();
    });

    test("User is logged in , role is operator , not Authenticated to delete bus ,  ,", async () => {
      const response = await request(app)
        .delete(`/trips/deleteTrip/${tripID}`)
        .set("Token", operator1Token);

      expect(response.status).toBe(400);
      expect(response.body.message).toBeDefined();
    });

    test("User is logged in , role is not operator ,", async () => {
      const response = await request(app)
        .delete(`/trips/deleteTrip/${tripID}`)
        .set("Token", customerToken);
      expect(response.status).toBe(403);
      expect(response.body.message).toBeDefined();
    });

    test("User is not  logged in", async () => {
      const response = await request(app)
        .delete(`/trips/deleteTrip/${tripID}`)
        .set("Token", "");

      expect(response.status).toBe(403);
      expect(response.body.error).toBeDefined();
    });
  });
});
