const app = require("../app");
const request = require("supertest");
const mongoose = require("mongoose");
// require("../dbConnect").connect()

const { connectDB, disconnectDB } = require("../dbConnectTest");

const {
  operator,
  validLoginOperator,
  customer,
  validLoginCustomer,
  operator1,
  validLoginOperator1,
} = require("../seedData/user.seed");

const {
  validBus,
  invalidBus1,
  busSeed,
  updateBus,
  busSeed1,
} = require("../seedData/bus.seed");

describe("Testing Bus", () => {
  let operatorToken = null;
  let customerToken = null;
  let operator1Token = null;
  let busID = null;
  let busID1 = null;
  let invalidBusID = "62ff1fa88396dc22711df949";

  beforeAll(async () => {
    await connectDB();
    await request(app).post("/users/register").send(operator);
    const loggedOpperator = await request(app)
      .post("/users/login")
      .send(validLoginOperator);
    operatorToken = loggedOpperator.body.token;

    await request(app).post("/users/register").send(customer);
    const loggedCustomer = await request(app)
      .post("/users/login")
      .send(validLoginCustomer);
    customerToken = loggedCustomer.body.token;
    console.log(customerToken);

    const bus = await request(app)
      .post("/bus")
      .set("Token", operatorToken)
      .send(busSeed);
    const bus1 = await request(app)
      .post("/bus")
      .set("Token", operatorToken)
      .send(busSeed1);
    busID1 = bus1.body.id;
    busID = bus.body.id;
    console.log(busID);

    await request(app).post("/users/register").send(operator1);
    const loggedOperator1 = await request(app)
      .post("/users/login")
      .send(validLoginOperator1);
    operator1Token = loggedOperator1.body.token;
  });

  afterAll(async () => {
    await disconnectDB();
  });

  describe("Testing the Add bus", () => {
    test("If role is operator , bus details are valid and registration number is unique", async () => {
      const response = await request(app)
        .post("/bus")
        .set("Token", operatorToken)
        .send(validBus);

      expect(response.status).toBe(200);
    });

    test("If role is operator , bus details are valid and registration number is not unique", async () => {
      const response = await request(app)
        .post("/bus")
        .set("Token", operatorToken)
        .send(validBus);

      expect(response.status).toBe(400);
    });

    test("If role is operator , bus details are missing  ", async () => {
      const response = await request(app)
        .post("/bus")
        .set("Token", operatorToken)
        .send(invalidBus1);

      expect(response.status).toBe(403);
      expect(response.body.message).toBeDefined();
    });

    test("If role is not operator  ", async () => {
      console.log(customerToken);
      const response = await request(app)
        .post("/bus")
        .set("Token", customerToken)
        .send(validBus);

      expect(response.status).toBe(403);
    });
  });

  describe("Testing the getting bus", () => {
    test("If role is operator and has added buses ", async () => {
      const response = await request(app)
        .get("/bus")
        .set("Token", operatorToken);

      expect(response.status).toBe(200);
      expect(response.body.bus).toBeDefined();
    });

    test("If role is operator and not added buses ", async () => {
      const response = await request(app)
        .get("/bus")
        .set("Token", operator1Token);

      expect(response.status).toBe(400);
      expect(response.error).toBeDefined();
    });

    test("If role is not operator ", async () => {
      const response = await request(app)
        .get("/bus")
        .set("Token", customerToken);

      expect(response.status).toBe(403);
      expect(response.error).toBeDefined();
      expect(response.body.message).toBeDefined();
    });
  });

  describe("Testing the getting bus by ID", () => {
    test("If role is operator , has added bus and bus id is valid  ", async () => {
      console.log(busID);
      const response = await request(app)
        .get(`/bus/${busID}`)
        .set("Token", operatorToken);

      expect(response.status).toBe(200);
      expect(response.body.bus).toBeDefined();
    });

    test("If role is operator , has not added bus  ", async () => {
      const response = await request(app)
        .get(`/bus/${busID}`)
        .set("Token", operator1Token);

      expect(response.status).toBe(403);
      expect(response.error).toBeDefined();
    });

    test("If bus id is invalid ", async () => {
      const response = await request(app)
        .get(`/bus/${invalidBusID}`)
        .set("Token", operatorToken);

      expect(response.status).toBe(400);
      expect(response.error).toBeDefined();
    });

    describe("Testing for Updating Bus", () => {
      test("valid authorization to update ", async () => {
        const response = await request(app)
          .patch(`/bus/${busID}`)
          .set("Token", operatorToken)
          .send(updateBus);

        expect(response.status).toBe(200);
        expect(response.body.message).toBeDefined();
      });

      test("invalid authorization to update ", async () => {
        const response = await request(app)
          .patch(`/bus/${busID}`)
          .set("Token", operator1Token)
          .send(updateBus);

        expect(response.status).toBe(403);
        expect(response.body.err).toBeDefined();
      });
    });

    describe("Testing for deleting bus", () => {
      test("valid  authorization ", async () => {
        const response = await request(app)
          .delete(`/bus/${busID}`)
          .set("Token", operatorToken);

        expect(response.status).toBe(200);
        expect(response.body.message).toBeDefined();
      });

      test(" invalid  authorization ", async () => {
        console.log(busID1);
        const response = await request(app)
          .delete(`/bus/${busID1}`)
          .set("Token", operator1Token);

        expect(response.status).toBe(403);
        expect(response.body.err).toBeDefined();
      });

      test(" invalid  busid ", async () => {
        const response = await request(app)
          .delete(`/bus/${invalidBus1}`)
          .set("Token", operatorToken);

        expect(response.status).toBe(400);
        expect(response.body.error).toBeDefined();
      });
    });
  });
});
