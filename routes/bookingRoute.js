const express = require('express')
const router = express.Router()


const {requireAuth} = require('../middleWare/authMiddleWare')
const {checkSeats,isValidTripId,isTripEnded,isValidBookingId,getquery} = require('../middleWare/bookingsMiddleWare')
const {bookBus,cancelBus,getAllBookings,searchByPlace} = require('../controller/bookingsControllers')



router.get('/',getquery,searchByPlace)//Get List of Bus based on Source and Destination... 
router.post('/bookBus',requireAuth,isValidTripId,checkSeats,bookBus)// Book the Bus Ticket
router.delete('/cancelBus/:id',requireAuth,isValidBookingId,isTripEnded,cancelBus) // Cancel the ticket
router.get('/getAllBookings',requireAuth,getAllBookings)// Get list of all tickets booked by the User

module.exports = router