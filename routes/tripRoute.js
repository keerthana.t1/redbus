const express = require('express')
const router = express.Router()

const {addTrip,getAllTrip,deleteTrip,getAllTripOperator,updateTrip} = require('../controller/busTripController')
const {isValidToAddTrip,checkTripDate,checkSeatBooked,isValidToDeleteTrip,checkOverlap,validateTrip,isvalidDates}  = require('../middleWare/busTripMiddleWare')
const {appendOperatorIdToBody} = require("../middleWare/busMiddleWare")
const {requireAuth,getRole} = require('../middleWare/authMiddleWare')



router.post('/addTrip/:id', requireAuth, getRole,isValidToAddTrip,validateTrip,appendOperatorIdToBody,isvalidDates,checkTripDate,addTrip)
router.get('/getAllTrip/:id', requireAuth, getRole,getAllTrip)
router.get('/getAllTripOperator',requireAuth, getRole,getAllTripOperator)
router.delete('/deleteTrip/:id',requireAuth, getRole,isValidToDeleteTrip,checkSeatBooked,deleteTrip)
router.put('/updateTrip/:id',requireAuth, getRole,isValidToDeleteTrip,validateTrip,checkSeatBooked,checkOverlap,updateTrip)

checkOverlap,updateTrip
module.exports = router