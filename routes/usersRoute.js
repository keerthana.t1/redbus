const express = require('express')
const router = express.Router()
const {register,login} = require('../controller/userControlller')
const {validateRegister,validateLogin,checkMail} = require('../middleWare/userMiddleWare')

router.post('/register',validateRegister,checkMail,register)
router.post('/login',validateLogin,login)

module.exports = router