const express = require('express')
const router = express.Router()


const {requireAuth,validateBus,isValidUser,validateTrip} = require('../middleWare/authMiddleWare')
const {appendOperatorIdToBody,getBusById,isBusExists,getRole} = require('../middleWare/busMiddleWare')
const {insert, updateBus,deleteBus,getAllBus,getbus} = require("../controller/busController")
const {addTrip,getAllTrip,deleteTrip,getAllTripOperator,updateTrip} = require('../controller/busTripController')
const {isValidToAddTrip,checkTripDate,checkSeatBooked,isValidToDeleteTrip,checkOverlap}  = require('../middleWare/busTripMiddleWare')



router.post('/', requireAuth, getRole, validateBus,appendOperatorIdToBody, isBusExists,insert)//Add Bus
router.patch('/:id',requireAuth , getRole ,getBusById,isValidUser,updateBus)// Update Bus details
router.delete('/:id',requireAuth,getRole,getBusById,isValidUser,deleteBus)//Delete the Bus
router.get('/',requireAuth , getRole , appendOperatorIdToBody , getAllBus)// Get list of Bus added by the Operator
router.get('/:id',requireAuth , getRole , getBusById,isValidUser, getbus) 



module.exports = router