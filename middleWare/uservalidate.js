const Joi = require("joi");

exports.resgiterSchemaValidator = Joi.object({
  name: Joi.string()

    .min(3)
    .max(30)
    .required(),
  mail: Joi.string().required().email({ minDomainSegments: 2 }),

  password:
    Joi.string()
    .required(),
  role: Joi.string().valid("customer", "busOperator").required(),
});

exports.loginSchemaValidator = Joi.object({
  mail: Joi.string().required().email({ minDomainSegments: 2 }),

  password: Joi.string().required(),
});
