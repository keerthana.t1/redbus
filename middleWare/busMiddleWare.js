var BusModel = require("../models/busModel");
const { checkDetails } = require("../Services/userDb");
const { findBus, isValidBusId } = require("../Services/busDb");

exports.appendOperatorIdToBody = async (req, res, next) => {
  const operatorId = req.auth.user._id;
  req.body.busOperatorId = operatorId;
  console.log("appendoperatoridtobody....\n" + req.body.busOperatorId);
  next();
};

exports.getBusById = async (req, res, next) => {
  try {
    console.log(req.params.id);
    const bus = await isValidBusId(req.params.id);
    req.bus = bus;
    if (bus) {
      req.bus = bus;
      console.log("GetBusByID.......\nBus Details : " + req.bus);
      return next();
    }
    return res.status(400).json({
      error: "no bus in db",
    });
  } catch (e) {
    console.log(`Error in geting bus by id : `, e);
    return res.status(400).json({
      error: "no bus in db",
    });
  }
};

exports.isUpdatable = async (req, res, next) => {
  try {
    const operatorId = req.params.id;
    console.log("isupdatable" + req.params.id);

    const bus = await BusModel.findById(req.params.id);
    if (bus.bookedSeat != bus.totalSeats) {
      next();
    }
  } catch (err) {
    console.log(`Error in geting bus by id : `, err);
    return res.status(400).json({
      error: "no bus in db",
    });
  }
};

exports.isBusExists = async (req, res, next) => {
  const registrationNo = req.body.registrationNo;

  const bus = await findBus(registrationNo);
  if (bus == "") {
    return next();
  }
  return res.status(400).json({
    error: "Registration No exists",
  });
};

exports.getRole = async (req, res, next) => {
  const { mail, role } = req.auth.user;
  console.log(mail);
  const result = await checkDetails(mail);
  const busOperatorId = result._id;
  console.log("Get Role......");
  console.log(
    "Mail ID : " +
      mail +
      "\n role : " +
      role +
      "\n busOperatorId : " +
      busOperatorId
  );
  if (role != "busOperator") {
    return res.status(403).json({
      err: "error occured",
      message: "Not valid User",
    });
  } else {
    console.log("next");
    next();
  }
};
