const Joi = require("joi");
Joi.objectId = require("joi-objectid")(Joi);

exports.busSchemaValidator = Joi.object({
  name: Joi.string()

    .min(3)
    .max(30)
    .required(),
  source: Joi.string().required(),

  destination:
    Joi.string()
    .required(),
  amount: Joi.number()
    .integer()

    .required(),

  type: Joi.string().valid("NORMAL", "SEMI-SLEEPER", "SLEEPER").required(),

  isAC: Joi.boolean()
  .required(),

  registrationNo: Joi.string().required(),
});
