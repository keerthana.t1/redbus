const BusTrip = require("../models/busTripModel");
const BusModel = require("../models/busModel");

const { checkTrip, findTripById } = require("../Services/tripDb");
const { busTripSchemaValidator } = require("./busTripValidator");
const { isValidBusId } = require("../Services/busDb");

exports.isvalidDates = async (req, res, next) => {
  try {
    console.log("ivalid date");
    const { startDate, endDate } = req.body;
    const present = new Date().toISOString();

    console.log(present);
    if (startDate > present && endDate > present) {
      return next();
    } else {
      // #swagger.responses[400] = { description: 'Not valid' }
      return res.status(400).json({
        error: "Dates are invalid",
      });
    }
  } catch (error) {
    return res.status(400).json({
      error: "error occured",
      err: error.message,
    });
  }
};

exports.isValidToAddTrip = async (req, res, next) => {
  const bus = await isValidBusId(req.params.id);
  // const bus_id = req.params.id
  // const bus = await BusModel.findById({_id:`${bus_id}`})
  console.log("isvalidtoaddtrip" + bus);
  const operatorId = bus.busOperatorId;
  if (bus && String(req.auth.user._id) === String(operatorId)) {
    next();
  } else {
    // #swagger.responses[400] = { description: 'Not valid' }
    return res.status(400).json({
      error: "Not Valid to add trip",
    });
  }
};

exports.checkTripDate = async (req, res, next) => {
  const busId = req.params.id;
  const { startDate, endDate } = req.body;
  console.log(startDate, endDate);

  const trip = await checkTrip(busId, startDate, endDate);
  
  if (trip) {
    // #swagger.responses[400] = { description: 'Not valid' }
    return res.status(400).json({
      message: "A trip exists on the same date",
    });
  }

  next();
};

exports.checkSeatBooked = async (req, res, next) => {
  console.log("checking if seats are booked" + req.trip.bookedSeats);
  if (req.trip.bookedSeats > 0) {
    return res.status(400).json({
      message: "Seats are booked cannot update",
    });
  }
  next();
};

exports.isValidToDeleteTrip = async (req, res, next) => {
  try {
    const tripId = req.params.id;
    const trip = await findTripById(tripId);
    console.log(trip);

    if (trip && String(trip.busOperatorId) === String(req.auth.user._id)) {
      req.trip = trip;
      return next();
    } else {
      return res.status(400).json({
        message: "Access denied ",
      });
    }
  } catch (err) {
    return res.status(400).json({
      err: err.message,
    });
  }
};

exports.checkOverlap = async (req, res, next) => {
  try {
    const busId = req.trip.busId;
    const currentStartDate = req.body.startDate;
    const currentEndDate = req.body.endDate;
    const trip = await checkTrip(busId, currentStartDate, currentEndDate);
    if (trip) {
      if (trip.id == req.trip.id) {
        return next();
      } else {
        return res.status(400).json({
          message: "A trip already exits , please change start date/end date",
        });
      }
    }
    next();
  } catch (err) {
    return res.status(400).json({
      err: err.message,
    });
  }
};

exports.validateTrip = (req, res, next) => {
  const { error } = busTripSchemaValidator.validate(req.body);
  console.log(error);
  if (error) {
    // #swagger.responses[400] = { description: 'Not valid' }
    return res.status(400).json({
      message: `Error in User Data : ${error.message}`,
    });
  } else {
    console.log("working");
    next();
  }
};
