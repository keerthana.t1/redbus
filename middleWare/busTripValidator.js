const Joi = require("joi");
Joi.objectId = require("joi-objectid")(Joi);

exports.busTripSchemaValidator = Joi.object({
  name: Joi.string()

    .min(3)
    .max(30)
    .required(),
  source: Joi.string().required(),

  destination:
    Joi.string()
    .required(),
  amount: Joi.number()
    .integer()

    .required(),

  totalSeats: Joi.number()
    .integer()

    .required(),
  bookedSeats: Joi.number().integer(),

  type: Joi.string().valid("NORMAL", "SEMI-SLEEPER", "SLEEPER").required(),

  isAC: Joi.boolean()
  .required(),

  startDate: Joi.date().iso().required(),

  endDate: Joi.date().iso().required(),
});
