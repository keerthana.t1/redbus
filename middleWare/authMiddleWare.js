const jwt = require("jsonwebtoken");
const User = require("../models/user");
const BusModel = require("../models/busModel");

const { isValidBusId } = require("../Services/busDb");
const { busSchemaValidator } = require("./busValidate");
const { busTripSchemaValidator } = require("./busTripValidator");


exports.requireAuth = (req, res, next) => {
  console.log("Inside requireauth ");
  const token = req.header("Token");
  console.log(token);
  if (!token) {
    // #swagger.responses[403] = { description: 'User Not logged in ' }

    return res.status(403).json({
      error: "not a valid user",
    });
  }
  try {
    const decode = jwt.verify(token, process.env.SECRET_KEY);
    req.auth = decode;
    console.log(req.auth);
  } catch (error) {
    // #swagger.responses[401] = { description: 'Invalid Token' }
    return res.status(401).send("Invalid user token" + error);
  }
  return next();
};

exports.getRole = async (req, res, next) => {
  const { role } = req.auth.user;

  if (role != "busOperator") {
    // #swagger.responses[403] = { description: 'User is not a operator' }
    return res.status(403).json({
      err: "error occured",
      message: "Not valid User",
    });
  } else {
    console.log("next");
    next();
  }
};

exports.isValidUser = async (req, res, next) => {
  const bus = await isValidBusId(req.params.id);
  // const bus = await BusModel.findById({_id: `${req.params.id}` })
  // console.log("bus"+bus)
  if (bus) {
    const paramOpId = req.bus.busOperatorId;
    const tokenOpID = req.auth.user._id;
    console.log("req.body.busOperatorId : " + paramOpId);
    console.log("req.auth.obj._id : " + tokenOpID);
    if (String(paramOpId) === String(tokenOpID)) {
      console.log("Operator ID from DB and token are equal");
      next();
    } else {
      return res.status(403).json({
        err: "access denied",
      });
    }
  } else {
    return res.status(400).json({
      message: "Error in busid",
    });
  }
};

exports.validateBus = (req, res, next) => {
  const { error } = busSchemaValidator.validate(req.body);
  console.log(error);
  if (error) {
    return res.status(403).json({
      message: `Error in User Data : ${error.message}`,
    });
  } else {
    console.log("working");
    next();
  }
};

exports.validateTrip = (req, res, next) => {
  const { error } = busTripSchemaValidator.validate(req.body);
  console.log(error);
  if (error) {
    return res.status(400).json({
      message: `Error in User Data : ${error.message}`,
    });
  } else {
    console.log("working");
    next();
  }
};
