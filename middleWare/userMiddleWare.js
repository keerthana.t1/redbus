const User = require("../models/user");
const {
  resgiterSchemaValidator,
  loginSchemaValidator,
} = require("./uservalidate");
const { checkDetails } = require("../Services/userDb");

exports.checkMail = async (req, res, next) => {
  try {
    const user = await checkDetails(req.body.mail);
    console.log("checkmail" + user);
    if (!user) {
      return next();
    }
    // #swagger.responses[400] = { description: 'Mail ID already exists' }
    else {
      return res.status(400).json({
        message: "Mail alreay Exists",
      });
    }
  } catch (error) {
    return res.status(400).json({
      Error: "Error in check mail",
    });
  }
};

exports.validateRegister = async (req, res, next) => {
  console.log("validating register");

  const { error } = resgiterSchemaValidator.validate(req.body);
  // #swagger.responses[406] = { description: 'Error in user Data' }
  if (error) {
    return res.status(406).json({
      message: `Error in User Data : ${error.message}`,
    });
  } else {
    console.log("working");
    return next();
  }
};

exports.validateLogin = (req, res, next) => {
  const { error } = loginSchemaValidator.validate(req.body);

  if (error) {
    // #swagger.responses[400] = { description: 'Error in user data' }
    return res.status(400).json({
      message: `Error in User Data : ${error.message}`,
    });
  } else {
    console.log("working");
    return next();
  }
};
