const mongoose = require("mongoose");
const seatSchema = require("../models/seatModel");
const { findTripById } = require("../Services/tripDb");
const { findByBookingId } = require("../Services/bookingsDb");

const updateSeats = (seats, seatNo) => {
  for (let seat of seats) {
    for (let no of seatNo) {
      if (seat.seatNo === no) {
        seat.isBooked = !seat.isBooked;
      }
    }
  }
  console.log("seats after updating" + seats);
  return seats;
};

const allocateSeats = (noOfSeats, seats) => {
  let freeSeatsCount = 0;
  let freeSeatNo = [];
  console.log(seats);

  for (let seat of seats) {
    console.log("for loop running" + !seat.isBooked);
    if (!seat.isBooked) {
      freeSeatsCount++;
      freeSeatNo.push(seat.seatNo);

      console.log(freeSeatNo);
    }
    if (freeSeatsCount == noOfSeats) {
      let status = true;

      return {
        freeSeatNo,
        status,
      };
    }
  }

  return {
    status: false,
  };
};

exports.checkSeats = async (req, res, next) => {
  try {
    console.log("User details from token\n" + req.auth);
    console.log("bus id from query:" + req.query.tripId);
    console.log("seats from query:" + req.query.noOfSeats);

    const { tripId, noOfSeats } = req.query;
    console.log("bus id " + tripId);

    const bus = await findTripById(tripId);
    req.bus = bus;

    const availableSeats = bus.totalSeats - bus.bookedSeats;

    console.log(bus);
    const { freeSeatNo, status } = allocateSeats(noOfSeats, req.bus.seat);

    if (
      !(bus.bookedSeats === bus.totalSeats || noOfSeats > availableSeats) &&
      status
    ) {
      req.freeSeatNo = freeSeatNo;
      req.seat = updateSeats(req.bus.seat, freeSeatNo);
      return next();
    } else {
      return res.status(403).json({
        message: "seats fully booked",
      });
    }
  } catch (err) {
    res.send("errorrr in catch" + err);
  }
};

exports.isValidTripId = async (req, res, next) => {
  try {
    const tripId = req.query.tripId;
    console.log(tripId);

    const trip = await findTripById(tripId);
    console.log(trip);

    if (trip) {
      return next();
    }
    return res.status(400).json({
      message: "invalid tripID",
    });
  } catch (err) {
    res.send("error in isvalidId" + err);
  }
};

exports.isTripEnded = async (req, res, next) => {
  const endDate = req.ticket.endDate;
  const currentDate = new Date();
  console.log("comparing dates " + endDate);

  if (endDate < currentDate) {
    return res.status(400).json({
      err: `Cannot cancel this trip as it has already ended`,
    });
  }

  next();
};

exports.isValidBookingId = async (req, res, next) => {
  const bookingId = req.params.id;
  console.log(req.params.id);
  const ticket = await findByBookingId(bookingId);
  console.log(ticket);
  if (ticket == null) {
    return res.status(400).json({
      message: "Invalid Booking id ",
    });
  }
  req.ticket = ticket;
  next();
};

exports.getquery = async (req, res, next) => {
  try {
    const { source, destination, type, isAC } = req.query;
    if (source === undefined || destination === undefined) {
      return res.status(400).json({
        message: "Source or destination cannot be null!!!",
      });
    }
    var query = {
      source: source,
      destination: destination,
    };

    if (type) {
      query.type = type;
    }
    if (isAC) {
      query.isAC = isAC;
    }
    req.value = query;
    next();
  } catch (error) {
    return res.status(400).json({
      err: `error in getting query`,
    });
  }
};
