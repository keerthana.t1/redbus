// module.exports = {
//     "collectCoverageFrom": ["src/**/*.js", "!**/node_modules/**"],
//     "coverageReporters": ["html", "text", "text-summary", "cobertura"],
//     "testMatch": ["**/*.test.js"]
//   }

//   module.exports = {
//     preset: 'ts-jest',
//     testEnvironment: 'node',
//     testPathIgnorePatterns: ['.js'],
//     modulePathIgnorePatterns: ['common'],
//     collectCoverageFrom: ['**/*.ts', '!**/node_modules/**'],
//     coverageReporters: ['html', 'text', 'text-summary', 'cobertura'],
//     testTimeout: 60000,
// }

module.exports = {
  collectCoverageFrom: ["src/**/*.js", "!**/node_modules/**"],
  coverageReporters: ["html", "text", "text-summary", "cobertura"],
  testMatch: ["**/*.test.js"],
  testTimeout: 60000000,
};

